﻿using System;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Sasw.TestSupport;
using Xunit;

namespace Sasw.EasyQr.IntegrationTests.DependencyInjectionTests
{
    public static class AddEasyQrTests
    {
        public class Given_Dependency_Injection_When_Adding_EasyQr
            : Given_When_Then_Test
        {
            private Exception _exception = null!;

            protected override void Given()
            {
            }

            protected override void When()
            {
                try
                {
                    _ =
                        new ServiceCollection()
                            .AddEasyQr()
                            .BuildServiceProvider(
                                new ServiceProviderOptions
                                {
                                    ValidateOnBuild = true,
                                    ValidateScopes = true
                                });
                }
                catch (Exception exception)
                {
                    _exception = exception;
                }
            }

            [Fact]
            public void Then_It_Should_Not_Throw_An_Exception()
            {
                _exception.Should().BeNull();
            }
        }
    }
}