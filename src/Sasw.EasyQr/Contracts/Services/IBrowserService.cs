﻿using System.Threading.Tasks;

namespace Sasw.EasyQr.Contracts.Services
{
    public interface IBrowserService
    {
        Task OpenUri(string url);
    }
}