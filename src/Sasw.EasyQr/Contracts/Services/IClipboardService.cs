﻿using System.Threading.Tasks;

namespace Sasw.EasyQr.Contracts.Services
{
    public interface IClipboardService
    {
        Task SetClipboardText(string text);
    }
}