﻿using System.Threading.Tasks;

namespace Sasw.EasyQr.Contracts.Services
{
    public interface IShareService
    {
        Task ShareFile(string fileName, byte[] bytes);
        Task ShareText(string text);
    }
}