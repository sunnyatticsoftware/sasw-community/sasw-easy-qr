﻿namespace Sasw.EasyQr.Contracts.Services
{
    public interface IDialogService
    {
        void ShowLoading();
        void HideLoading();
        void ShowToast(string message);
    }
}