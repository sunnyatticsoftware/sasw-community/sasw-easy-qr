﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Sasw.EasyQr.Models;

namespace Sasw.EasyQr.Contracts.Services
{
    public interface IHistoryService
    {
        Task<IEnumerable<HistoryItem>> GetItems();
        Task AddItem(HistoryItem historyItem);
        Task ClearAll();
    }
}