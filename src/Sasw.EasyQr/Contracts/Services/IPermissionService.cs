﻿using System.Threading.Tasks;

namespace Sasw.EasyQr.Contracts.Services
{
    public interface IPermissionService
    {
        Task RequestScannerPermission();
    }
}