﻿using System;
using System.Threading.Tasks;

namespace Sasw.EasyQr.Contracts.Services
{
    public interface IPopupService
    {
        Task ShowScanner(Func<string, Task> onCodeScanned, Func<Task> onClosing, bool autoClose = false);
    }
}