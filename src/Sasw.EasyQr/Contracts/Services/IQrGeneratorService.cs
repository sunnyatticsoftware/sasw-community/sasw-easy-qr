﻿namespace Sasw.EasyQr.Contracts.Services
{
    public interface IQrGeneratorService
    {
        byte[] GetQrCode(string text);
    }
}