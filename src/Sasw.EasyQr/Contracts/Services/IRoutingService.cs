﻿using System.Threading.Tasks;

namespace Sasw.EasyQr.Contracts.Services
{
    public interface IRoutingService
    {
        Task NavigateTo(string route);
    }
}