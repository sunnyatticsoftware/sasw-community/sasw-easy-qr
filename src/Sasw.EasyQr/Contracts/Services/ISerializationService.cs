﻿namespace Sasw.EasyQr.Contracts.Services
{
    public interface ISerializationService
    {
        T Deserialize<T>(string json);
        string Serialize(object obj);
    }
}