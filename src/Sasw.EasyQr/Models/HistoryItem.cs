﻿using System;

namespace Sasw.EasyQr.Models
{
    public class HistoryItem
    {
        public string Text { get; set; } = string.Empty;
        public DateTime CreatedOn { get; set; } = DateTime.Now;
    }
}
