﻿using Microsoft.Extensions.DependencyInjection;
using Rg.Plugins.Popup.Services;
using Sasw.EasyQr.Contracts.Services;
using Sasw.EasyQr.Services;
using Sasw.EasyQr.ViewModels;

namespace Sasw.EasyQr
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddEasyQr(
            this IServiceCollection services)
        {
            services.AddSingleton<IRoutingService, RoutingService>();
            services.AddSingleton<IDialogService, DialogService>();
            services.AddSingleton<IBrowserService, BrowserService>();
            services.AddSingleton<IShareService, ShareService>();
            services.AddSingleton<IClipboardService, ClipboardService>();
            services.AddSingleton<ISerializationService, SerializationService>();

            services.AddTransient<IPermissionService, PermissionService>();
            services.AddTransient<IQrGeneratorService, QrGeneratorService>();
            services.AddTransient<IPopupService, PopupService>();
            services.AddTransient<IHistoryService, HistoryService>();
            services.AddTransient(sp => PopupNavigation.Instance);

            services.AddTransient<InfoPageViewModel>();
            services.AddTransient<QrScannerViewModel>();
            services.AddTransient<QrGeneratorViewModel>();
            services.AddTransient<HistoryViewModel>();

            return services;
        }
    }
}
