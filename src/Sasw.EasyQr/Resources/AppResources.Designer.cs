﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Sasw.EasyQr.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class AppResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal AppResources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Sasw.EasyQr.Resources.AppResources", typeof(AppResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        public static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Clear History.
        /// </summary>
        public static string ClearHistory {
            get {
                return ResourceManager.GetString("ClearHistory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Copied.
        /// </summary>
        public static string Copied {
            get {
                return ResourceManager.GetString("Copied", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Developed by.
        /// </summary>
        public static string InfoDevelopedBy {
            get {
                return ResourceManager.GetString("InfoDevelopedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to in Malaga, Spain.
        /// </summary>
        public static string InfoDevelopedIn {
            get {
                return ResourceManager.GetString("InfoDevelopedIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to More info.
        /// </summary>
        public static string InfoMoreInfo {
            get {
                return ResourceManager.GetString("InfoMoreInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Loading.
        /// </summary>
        public static string Loading {
            get {
                return ResourceManager.GetString("Loading", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to History.
        /// </summary>
        public static string MenuHistory {
            get {
                return ResourceManager.GetString("MenuHistory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Info.
        /// </summary>
        public static string MenuInfo {
            get {
                return ResourceManager.GetString("MenuInfo", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to QR Code.
        /// </summary>
        public static string MenuQrCode {
            get {
                return ResourceManager.GetString("MenuQrCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to QR Generator.
        /// </summary>
        public static string MenuQrGenerator {
            get {
                return ResourceManager.GetString("MenuQrGenerator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to QR Scanner.
        /// </summary>
        public static string MenuQrScanner {
            get {
                return ResourceManager.GetString("MenuQrScanner", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Click button to scan QR code.
        /// </summary>
        public static string PlaceholderClickScan {
            get {
                return ResourceManager.GetString("PlaceholderClickScan", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter text.
        /// </summary>
        public static string PlaceholderEnterText {
            get {
                return ResourceManager.GetString("PlaceholderEnterText", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Scan.
        /// </summary>
        public static string Scan {
            get {
                return ResourceManager.GetString("Scan", resourceCulture);
            }
        }
    }
}
