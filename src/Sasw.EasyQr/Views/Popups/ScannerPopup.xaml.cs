﻿using System.Linq;
using GoogleVisionBarCodeScanner;
using Sasw.EasyQr.ViewModels.Popups;
using Xamarin.Forms.Xaml;

namespace Sasw.EasyQr.Views.Popups
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScannerPopup
    {
        private readonly ScannerPopupViewModel _viewModel;

        public ScannerPopup(ScannerPopupViewModel scannerPopupViewModel)
        {
            InitializeComponent();
            _viewModel = scannerPopupViewModel;
            BindingContext = scannerPopupViewModel;
        }

        private async void CameraView_OnDetected(object sender, OnDetectedEventArg e)
        {
            var barcodeResults = e.BarcodeResults;
            var barcode = barcodeResults.FirstOrDefault();
            await _viewModel.ProcessBarcode(barcode.DisplayValue);
            Methods.SetIsScanning(false);
        }
    }
}