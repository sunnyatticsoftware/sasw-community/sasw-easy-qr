﻿using Sasw.EasyQr.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sasw.EasyQr.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QrScannerPage 
        : ContentPage
    {
        private readonly QrScannerViewModel _qrScannerViewModel = AppContainer.Resolve<QrScannerViewModel>();

        public QrScannerPage()
        {
            InitializeComponent();
            BindingContext = _qrScannerViewModel;
        }
    }
}