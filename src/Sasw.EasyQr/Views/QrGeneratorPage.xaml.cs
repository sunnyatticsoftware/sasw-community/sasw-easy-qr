﻿using Sasw.EasyQr.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sasw.EasyQr.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QrGeneratorPage 
        : ContentPage
    {
        private readonly QrGeneratorViewModel _qrGeneratorViewModel = AppContainer.Resolve<QrGeneratorViewModel>();

        public QrGeneratorPage()
        {
            InitializeComponent();
            BindingContext = _qrGeneratorViewModel;
        }
    }
}