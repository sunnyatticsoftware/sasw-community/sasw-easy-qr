﻿using Sasw.EasyQr.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sasw.EasyQr.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HistoryPage 
        : ContentPage
    {
        private readonly HistoryViewModel _historyViewModel = AppContainer.Resolve<HistoryViewModel>();

        public HistoryPage()
        {
            InitializeComponent();
            BindingContext = _historyViewModel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await _historyViewModel.Init();
        }
    }
}
