﻿using Sasw.EasyQr.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Sasw.EasyQr.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InfoPage 
        : ContentPage
    {
        private readonly InfoPageViewModel _infoViewModel = AppContainer.Resolve<InfoPageViewModel>();

        public InfoPage()
        {
            InitializeComponent();
            BindingContext = _infoViewModel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            await _infoViewModel.Init();
        }
    }
}