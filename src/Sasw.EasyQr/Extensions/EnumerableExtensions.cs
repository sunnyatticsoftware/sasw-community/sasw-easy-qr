﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Sasw.EasyQr.Extensions
{
    public static class EnumerableExtensions
    {
        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> collection)
            where T : class
        {
            var observableCollection = new ObservableCollection<T>();
            foreach (var originalItem in collection)
            {
                observableCollection.Add(originalItem);
            }

            return observableCollection;
        }
    }

}
