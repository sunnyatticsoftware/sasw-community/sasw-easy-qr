﻿using System.Threading.Tasks;
using Sasw.EasyQr.Contracts.Services;
using Xamarin.Essentials;

namespace Sasw.EasyQr.Services
{
    public class ClipboardService
        : IClipboardService
    {
        public Task SetClipboardText(string text)
        {
            return Clipboard.SetTextAsync(text);
        }
    }
}
