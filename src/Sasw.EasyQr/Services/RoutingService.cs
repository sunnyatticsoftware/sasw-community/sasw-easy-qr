﻿using System.Threading.Tasks;
using Sasw.EasyQr.Contracts.Services;
using Xamarin.Forms;

namespace Sasw.EasyQr.Services
{
    public class RoutingService
        : IRoutingService
    {
        public Task NavigateTo(string route)
        {
            return Shell.Current.GoToAsync(route);
        }
    }
}