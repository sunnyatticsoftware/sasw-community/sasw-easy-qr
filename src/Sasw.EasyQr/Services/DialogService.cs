﻿using System;
using Acr.UserDialogs;
using Sasw.EasyQr.Contracts.Services;
using Sasw.EasyQr.Resources;

namespace Sasw.EasyQr.Services
{
    public class DialogService
        : IDialogService
    {
        public void ShowLoading()
        {
            UserDialogs.Instance.ShowLoading(AppResources.Loading, MaskType.Clear);
        }

        public void HideLoading()
        {
            UserDialogs.Instance.HideLoading();
        }

        public void ShowToast(string message)
        {
            ToastConfig.DefaultPosition = ToastPosition.Top;
            ToastConfig.DefaultDuration = TimeSpan.FromMilliseconds(400);
            UserDialogs.Instance.Toast(message);
        }
    }
}
