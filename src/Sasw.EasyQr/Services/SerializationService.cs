﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Sasw.EasyQr.Contracts.Services;

namespace Sasw.EasyQr.Services
{
    public class SerializationService
        : ISerializationService
    {
        private readonly JsonSerializerSettings _settings =
            new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

        public T Deserialize<T>(string json)
        {
            var result = JsonConvert.DeserializeObject<T>(json, _settings);
            return result ?? throw new SerializationException($"Unexpected deserialization problem with Json: {json}");
        }

        public string Serialize(object obj)
        {
            var result = JsonConvert.SerializeObject(obj, _settings);
            return result;
        }
    }
}
