﻿using System.IO;
using System.Threading.Tasks;
using Sasw.EasyQr.Contracts.Services;
using Xamarin.Essentials;

namespace Sasw.EasyQr.Services
{
    public class ShareService
        : IShareService
    {
        public Task ShareFile(string fileName, byte[] bytes)
        {
            var file = Path.Combine(FileSystem.CacheDirectory, fileName);
            File.WriteAllBytes(file, bytes);
            var shareFile = new ShareFile(file);
            var shareFileRequest = new ShareFileRequest(fileName, shareFile);
            return Share.RequestAsync(shareFileRequest);
        }

        public Task ShareText(string text)
        {
            var shareTextRequest = new ShareTextRequest(text);
            return Share.RequestAsync(shareTextRequest);
        }
    }
}
