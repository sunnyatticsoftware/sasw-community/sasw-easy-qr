﻿using System.Threading.Tasks;
using GoogleVisionBarCodeScanner;
using Sasw.EasyQr.Contracts.Services;

namespace Sasw.EasyQr.Services
{
    public class PermissionService
        : IPermissionService
    {
        public async Task RequestScannerPermission()
        {
            await Methods.AskForRequiredPermission();
        }
    }
}
