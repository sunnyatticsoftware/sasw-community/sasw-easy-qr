﻿using System;
using System.Threading.Tasks;
using Rg.Plugins.Popup.Contracts;
using Rg.Plugins.Popup.Pages;
using Sasw.EasyQr.Contracts.Services;
using Sasw.EasyQr.ViewModels;
using Sasw.EasyQr.ViewModels.Popups;
using Sasw.EasyQr.Views.Popups;

namespace Sasw.EasyQr.Services
{
    public class PopupService
        : IPopupService
    {
        private readonly IPopupNavigation _popupNavigation;
        private readonly IDialogService _dialogService;

        public PopupService(
            IPopupNavigation popupNavigation,
            IDialogService dialogService)
        {
            _popupNavigation = popupNavigation;
            _dialogService = dialogService;
        }

        public async Task ShowScanner(
            Func<string, Task> onCodeScanned, 
            Func<Task> onClosing, 
            bool autoClose = false)
        {
            async Task OnClose()
            {
                await onClosing.Invoke();
                await _popupNavigation.PopAsync();
            }

            var scannerPopupViewModel =
                new ScannerPopupViewModel(
                    _dialogService,
                    async barcode =>
                    {
                        await onCodeScanned.Invoke(barcode);
                        if (autoClose)
                        {
                            await OnClose();
                        }
                    },
                    OnClose);

            var page = new ScannerPopup(scannerPopupViewModel);
            await StartPopup(page, scannerPopupViewModel);
        }

        private async Task StartPopup(PopupPage page, BasePopupViewModel viewModel)
        {
            await _popupNavigation.PushAsync(page);
            await viewModel.Init();
        }
    }
}
