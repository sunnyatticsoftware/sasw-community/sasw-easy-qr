﻿using QRCoder;
using Sasw.EasyQr.Contracts.Services;

namespace Sasw.EasyQr.Services
{
    public class QrGeneratorService
        : IQrGeneratorService
    {
        public byte[] GetQrCode(string text)
        {
            var qrGenerator = new QRCodeGenerator();
            var qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            var qRCode = new PngByteQRCode(qrCodeData);
            var qrCodeBytes = qRCode.GetGraphic(20);
            return qrCodeBytes;
        }
    }
}
