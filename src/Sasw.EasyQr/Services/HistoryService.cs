﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Sasw.EasyQr.Contracts.Services;
using Sasw.EasyQr.Models;
using Xamarin.Essentials;

namespace Sasw.EasyQr.Services
{
    public class HistoryService
        : IHistoryService
    {
        private readonly ISerializationService _serializationService;
        private const string HistoryKey = "History";
        private const string DefaultEmptyJson = "[]";

        public HistoryService(ISerializationService serializationService)
        {
            _serializationService = serializationService;
        }

        public Task<IEnumerable<HistoryItem>> GetItems()
        {
            var historyItems = GetAll();
            return Task.FromResult(historyItems);
        }

        public Task AddItem(HistoryItem historyItem)
        {
            var historyItems = GetAll();
            var items = historyItems.ToList();
            items.Add(historyItem);
            var json = _serializationService.Serialize(items);
            Preferences.Set(HistoryKey, json);
            return Task.CompletedTask;
        }

        public Task ClearAll()
        {
            Preferences.Set(HistoryKey, DefaultEmptyJson);
            return Task.CompletedTask;
        }

        private IEnumerable<HistoryItem> GetAll()
        {
            var json = Preferences.Get(HistoryKey, DefaultEmptyJson);
            var historyItems = _serializationService.Deserialize<IEnumerable<HistoryItem>>(json);
            return historyItems;
        }
    }
}
