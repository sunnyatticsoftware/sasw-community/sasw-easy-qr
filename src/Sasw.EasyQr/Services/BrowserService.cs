﻿using System.Threading.Tasks;
using Sasw.EasyQr.Contracts.Services;
using Xamarin.Essentials;

namespace Sasw.EasyQr.Services
{
    public class BrowserService
        : IBrowserService
    {
        public Task OpenUri(string url)
        {
            return Browser.OpenAsync(url);
        }
    }
}
