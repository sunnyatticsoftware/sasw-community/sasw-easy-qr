﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Sasw.EasyQr
{
    public class AppContainer
    {
        private static ServiceProvider _serviceProvider = new ServiceCollection().BuildServiceProvider();

        public static void Init()
        {
            var serviceProviderOptions =
                new ServiceProviderOptions
                {
                    ValidateOnBuild = true,
                    ValidateScopes = true
                };

            var serviceProvider =
                new ServiceCollection()
                    .AddEasyQr()
                    .BuildServiceProvider(serviceProviderOptions);

            _serviceProvider = serviceProvider;
        }

        public static object Resolve(Type typeName)
        {
            var requiredService = _serviceProvider.GetRequiredService(typeName);
            return requiredService;
        }

        public static T Resolve<T>() where T : class
        {
            var requiredService = _serviceProvider.GetRequiredService<T>();
            return requiredService;
        }
    }
}
