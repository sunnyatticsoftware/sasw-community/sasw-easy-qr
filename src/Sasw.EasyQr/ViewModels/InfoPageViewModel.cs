﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Sasw.EasyQr.Contracts.Services;
using Xamarin.Forms;

namespace Sasw.EasyQr.ViewModels
{
    public class InfoPageViewModel
        : BaseViewModel
    {
        private readonly IBrowserService _browserService;

        public InfoPageViewModel(
            IRoutingService routingService,
            IDialogService dialogService,
            IBrowserService browserService)
            : base(routingService, dialogService)
        {
            _browserService = browserService;
        }

        public string Version
        {
            get
            {
                var version = Assembly.GetExecutingAssembly().GetName().Version;
                var result = $"version {version.Major}.{version.Minor}";
                return result;
            }
        }
        public string Year => DateTime.UtcNow.Year.ToString();

        public ICommand TapViewWebsite => new Command(ViewWebsite);

        private async void ViewWebsite(object obj)
        {
            await _browserService.OpenUri(Constants.SunnyAtticSoftware.Url);
        }
        public override Task Init()
        {
            return Task.CompletedTask;
        }
    }
}
