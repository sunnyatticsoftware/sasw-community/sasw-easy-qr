﻿using Sasw.EasyQr.Contracts.Services;

namespace Sasw.EasyQr.ViewModels
{
    public abstract class BasePopupViewModel
        : BaseReactiveViewModel
    {
        protected BasePopupViewModel(IDialogService dialogService) 
            : base(dialogService)
        {
        }
    }
}
