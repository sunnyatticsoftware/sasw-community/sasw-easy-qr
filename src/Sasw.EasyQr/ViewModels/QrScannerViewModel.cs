﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Sasw.EasyQr.Contracts.Services;
using Sasw.EasyQr.Models;
using Sasw.EasyQr.Resources;
using Xamarin.Forms;

namespace Sasw.EasyQr.ViewModels
{
    public class QrScannerViewModel
        : BaseViewModel
    {
        private readonly IPermissionService _permissionService;
        private readonly IPopupService _popupService;
        private readonly IBrowserService _browserService;
        private readonly IClipboardService _clipboardService;
        private readonly IShareService _shareService;
        private readonly IHistoryService _historyService;
        private bool _isUrl;
        private string _textScanned = string.Empty;

        public QrScannerViewModel(
            IRoutingService routingService,
            IDialogService dialogService,
            IPermissionService permissionService,
            IPopupService popupService,
            IBrowserService browserService,
            IClipboardService clipboardService,
            IShareService shareService,
            IHistoryService historyService)
            : base(routingService, dialogService)
        {
            _permissionService = permissionService;
            _popupService = popupService;
            _browserService = browserService;
            _clipboardService = clipboardService;
            _shareService = shareService;
            _historyService = historyService;
        }

        public ICommand TapScan => new Command(Scan);
        public ICommand TapBrowse => new Command(Browse);
        public ICommand TapCopy => new Command(CopyToClipboard);
        public ICommand TapShare => new Command(Share);

        public string TextScanned
        {
            get => _textScanned;
            set
            {
                _textScanned = value;
                OnPropertyChanged(nameof(TextScanned));
            }
        }

        public bool IsUrl
        {
            get => _isUrl;
            set
            {
                _isUrl = value;
                OnPropertyChanged(nameof(IsUrl));
            }
        }

        public override Task Init()
        {
            return Task.CompletedTask;
        }

        private async void Scan(object obj)
        {
            await _permissionService.RequestScannerPermission();

            IsBusy = true;
            await _popupService.ShowScanner(
                code =>
                {
                    TextScanned = code;
                    IsUrl = Uri.TryCreate(code, UriKind.Absolute, out _);
                    _historyService.AddItem(
                        new HistoryItem
                        {
                            Text = code,
                            CreatedOn = DateTime.Now
                        });
                    return Task.CompletedTask;
                }, () => Task.CompletedTask, true);
        }

        private async void Browse(object obj)
        {
            await _browserService.OpenUri(TextScanned);
        }

        private async void CopyToClipboard(object obj)
        {
            await _clipboardService.SetClipboardText(TextScanned);
            ShowToast(AppResources.Copied);
        }

        private async void Share(object obj)
        {
            IsBusy = true;
            await _shareService.ShareText(TextScanned);
            IsBusy = false;
        }
    }
}
