﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using Sasw.EasyQr.Contracts.Services;
using Sasw.EasyQr.Models;
using Xamarin.Forms;

namespace Sasw.EasyQr.ViewModels
{
    public class QrGeneratorViewModel
        : BaseViewModel
    {
        private readonly IQrGeneratorService _qrGeneratorService;
        private readonly IShareService _shareService;
        private readonly IHistoryService _historyService;
        private string _text = string.Empty;
        private bool _isGenerated;
        private ImageSource _imageSource = null!;
        private byte[] _imageBytes = new byte[0];

        public QrGeneratorViewModel(
            IRoutingService routingService,
            IDialogService dialogService,
            IQrGeneratorService qrGeneratorService,
            IShareService shareService,
            IHistoryService historyService)
            : base(routingService, dialogService)
        {
            _qrGeneratorService = qrGeneratorService;
            _shareService = shareService;
            _historyService = historyService;
        }

        public ICommand TapGenerate => new Command(Generate);
        public ICommand TapClear => new Command(Clear);
        public ICommand TapShare => new Command(Share);

        public string Text
        {
            get => _text;
            set
            {
                _text = value;
                OnPropertyChanged(nameof(Text));
            }
        }

        public ImageSource ImageSource
        {
            get => _imageSource;
            set
            {
                _imageSource = value;
                OnPropertyChanged(nameof(ImageSource));
            }
        }

        public byte[] ImageBytes
        {
            get => _imageBytes;
            set
            {
                _imageBytes = value;
                OnPropertyChanged(nameof(ImageBytes));
            }
        }

        public bool IsGenerated
        {
            get => _isGenerated;
            set
            {
                _isGenerated = value;
                OnPropertyChanged(nameof(IsGenerated));
            }
        }

        public override Task Init()
        {
            IsBusy = false;
            return Task.CompletedTask;
        }

        private void Generate(object obj)
        {
            IsBusy = true;
            var imageBytes = _qrGeneratorService.GetQrCode(Text);
            var imageSource = ImageSource.FromStream(() => new MemoryStream(imageBytes));
            ImageBytes = imageBytes;
            ImageSource = imageSource;
            IsGenerated = true;
            _historyService.AddItem(
                new HistoryItem
                {
                    Text = Text,
                    CreatedOn = DateTime.Now
                });
            IsBusy = false;
        }

        private async void Share(object obj)
        {
            IsBusy = true;
            await _shareService.ShareFile($"easy-qr-{DateTime.UtcNow.Ticks}.png", ImageBytes);
            IsBusy = false;
        }

        private void Clear(object obj)
        {
            Text = string.Empty;
            ImageSource = null!;
            ImageBytes = new byte[0];
            IsGenerated = false;
        }
    }
}