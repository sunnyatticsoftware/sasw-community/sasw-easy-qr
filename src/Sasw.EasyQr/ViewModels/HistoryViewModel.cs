﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Sasw.EasyQr.Contracts.Services;
using Sasw.EasyQr.Extensions;
using Sasw.EasyQr.Models;
using Sasw.EasyQr.Resources;
using Xamarin.Forms;

namespace Sasw.EasyQr.ViewModels
{
    public class HistoryViewModel
        : BaseViewModel
    {
        private readonly IHistoryService _historyService;
        private readonly IClipboardService _clipboardService;
        private ObservableCollection<HistoryItem> _historyItems = new ObservableCollection<HistoryItem>();

        public HistoryViewModel(
            IRoutingService routingService,
            IDialogService dialogService,
            IHistoryService historyService,
            IClipboardService clipboardService)
            : base(routingService, dialogService)
        {
            _historyService = historyService;
            _clipboardService = clipboardService;
        }

        public ICommand TapClearHistory => new Command(ClearHistory);
        public ICommand TapCopy => new Command<HistoryItem>(Copy);

        public ObservableCollection<HistoryItem> HistoryItems
        {
            get => _historyItems;
            set
            {
                _historyItems = value;
                OnPropertyChanged(nameof(HistoryItems));
            }
        }

        public override async Task Init()
        {
            await LoadHistory();
        }

        private async void Copy(HistoryItem historyItem)
        {
            await _clipboardService.SetClipboardText(historyItem.Text);
            ShowToast(AppResources.Copied);
        }

        private async void ClearHistory(object obj)
        {
            await _historyService.ClearAll();
            await LoadHistory();
        }

        private async Task LoadHistory()
        {
            var historyItems = await _historyService.GetItems();
            HistoryItems = historyItems.Reverse().ToObservableCollection();
        }
    }
}