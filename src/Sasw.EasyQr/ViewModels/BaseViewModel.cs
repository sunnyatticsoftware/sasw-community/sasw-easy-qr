﻿using System.Threading.Tasks;
using Sasw.EasyQr.Contracts.Services;

namespace Sasw.EasyQr.ViewModels
{
    public abstract class BaseViewModel
        : BaseReactiveViewModel
    {
        private readonly IRoutingService _routingService;

        protected BaseViewModel(
            IRoutingService routingService,
            IDialogService dialogService)
            : base(dialogService)
        {
            _routingService = routingService;
        }

        protected Task GoTo(string route)
        {
            return _routingService.NavigateTo(route);
        }
    }
}
