﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Sasw.EasyQr.Contracts.Services;

namespace Sasw.EasyQr.ViewModels
{
    public abstract class BaseReactiveViewModel
        : IDisposable, INotifyPropertyChanged
    {
        private readonly IDialogService _dialogService;
        private bool _isBusy;

        protected BaseReactiveViewModel(
            IDialogService dialogService)
        {
            _dialogService = dialogService;
        }

        public bool IsBusy
        {
            get => _isBusy;
            set
            {
                _isBusy = value;
                if (value)
                {
                    _dialogService.ShowLoading();
                }
                else
                {
                    _dialogService.HideLoading();
                }
                OnPropertyChanged(nameof(IsBusy));
            }
        }

        protected void ShowToast(string message)
        {
            _dialogService.ShowToast(message);
        }

        public abstract Task Init();

        public event PropertyChangedEventHandler PropertyChanged = null!;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Dispose()
        {
        }
    }
}
