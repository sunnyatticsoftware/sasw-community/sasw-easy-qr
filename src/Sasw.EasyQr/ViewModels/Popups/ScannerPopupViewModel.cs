﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Sasw.EasyQr.Contracts.Services;
using Xamarin.Forms;

namespace Sasw.EasyQr.ViewModels.Popups
{
    public class ScannerPopupViewModel
        : BasePopupViewModel
    {
        private readonly Func<string, Task> _onBarcodeScanned;
        private readonly Func<Task> _onClose;

        public ScannerPopupViewModel(
            IDialogService dialogService,
            Func<string, Task> onBarcodeScanned,
            Func<Task> onClose)
            : base(dialogService)
        {
            _onBarcodeScanned = onBarcodeScanned;
            _onClose = onClose;
        }

        public ICommand TapClose => new Command(Close);

        public async Task ProcessBarcode(string barcode)
        {
            await _onBarcodeScanned.Invoke(barcode);
        }

        private async void Close(object obj)
        {
            await _onClose.Invoke();
        }

        public override Task Init()
        {
            IsBusy = false;
            return Task.CompletedTask;
        }
    }
}
