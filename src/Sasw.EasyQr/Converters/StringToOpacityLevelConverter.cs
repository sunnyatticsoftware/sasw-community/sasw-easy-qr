﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace Sasw.EasyQr.Converters
{
    public class StringToOpacityLevelConverter
        : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return !string.IsNullOrEmpty(value?.ToString())
                ? Constants.Style.ItemEnabledOpacityLevel
                : Constants.Style.ItemDisabledOpacityLevel;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
