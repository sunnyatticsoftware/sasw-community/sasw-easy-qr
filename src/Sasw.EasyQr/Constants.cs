﻿namespace Sasw.EasyQr
{
    public static class Constants
    {
        public static class Style
        {
            public const float ItemEnabledOpacityLevel = (float) 1.0;
            public const float ItemDisabledOpacityLevel = (float)0.3;
        }

        public static class SunnyAtticSoftware
        {
            public const string Url = "https://sunnyatticsoftware.com";
        }
    }
}
