﻿using Android.App;
using Android.Content.PM;
using Android.OS;

namespace Sasw.EasyQr.Droid
{
    [Activity(Label = "Easy QR", Icon = "@drawable/easyqr_launch_icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            //Popups
            Rg.Plugins.Popup.Popup.Init(this, savedInstanceState);

            //Scanner
            GoogleVisionBarCodeScanner.Droid.RendererInitializer.Init();

            LoadApplication(new App());
        }

        public override void OnBackPressed()
        {
            if (Rg.Plugins.Popup.Popup.SendBackPressed(base.OnBackPressed))
            {
                // Do something if there are some pages in the `PopupStack`
            }
        }
    }
}