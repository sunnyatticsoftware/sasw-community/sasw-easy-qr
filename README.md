# sasw-easy-qr

**Easy QR** is an open source, free and ad-free Android application developed with Xamarin Forms by Diego Martín at [Sunny Attic Software](https://sunnyatticsoftware.com)
to scan and generate QR codes.

## Download
Download Easy QR for Android [here](https://sunnyatticsoftware.com/blog/android-app-to-scan-and-generate-qr-codes).

## Features
- 100% Free
- 100% Ad-Free
- 100% Open source
- Multilingual EN, ES, IT, FR and DE
- QR Code scanning
- Copying and sharing scanned result
- QR Code generating from any text or Url
- QR image sharing

## Technology used
Some of the libraries and technologies used are:
- Xamarin Forms
- Xamarin Essentials
- Acr.UserDialogs
- BarcodeScanner.XF
- QRCoder
- Rg.Plugins.Popup
- Microsoft.Extensions.DependencyInjection

## Additional notes
This application is only available for Android and there are no plans to release it for iOS.

android-app-to-scan-and-generate-qr-codes).